module.exports = {
  theme: {
    extend: {
      spacing: {
        '2px': '2px',
      },
      'max-w-8xl': {
        'max-width': '90rem'
      }
    },
  },
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  plugins: [
    require("@tailwindcss/ui"),
    require("@tailwindcss/forms"),
  ],
}
