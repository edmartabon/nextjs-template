import { eventBus } from 'utils'

export default function Notification(info) {
  eventBus.dispatch('notification.add', info)
}