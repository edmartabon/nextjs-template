import cookie from 'js-cookie'

module.exports = {
  isLogged() {
    if (cookie.get('accessToken')) {
      return true
    }
    return false
  }
}