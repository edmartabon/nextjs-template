import axios from 'axios'
import cookie from 'js-cookie'

let config = {
  baseURL: process.env.NEXT_PUBLIC_API_URL+'/v1',
  header: {
    'Content-Type': 'application/vnd.api+json'
  }
}

const http = axios.create(config)

http.interceptors.request.use(config => {
  const token = cookie.get('accessToken')

  if (token) {
    config.headers.Authorization = 'bearer '+token
  }
  return config
})

http.interceptors.response.use(res => {
  return res
}, function (err) {
  return Promise.reject(err)
})

export default http