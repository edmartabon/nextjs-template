const colors = [
  'blue',
  'red',
  'orange',
  'yellow',
  'green',
  'teal',
  'blue',
  'indigo',
  'purple',
  'pink',
]

export default function avatar(firstName, lastName) {
  const firstChar = (firstName.charAt(0)).charCodeAt(0) - 65;
  const firstNum = firstChar.toString().charAt(0)

  return {
    prefix: firstName.charAt(0).toUpperCase() + lastName.charAt(0).toUpperCase(),
    color: colors[firstNum]
  }
}