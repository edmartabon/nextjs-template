import { UsersIcon, HomeIcon, CogIcon } from '@heroicons/react/outline'

export default [
  { name: 'Home', href: '/', icon: HomeIcon },
  { name: 'Users', href: '/users', icon: UsersIcon },
  { name: 'Properties', href: '/properties', icon: HomeIcon },
  { name: 'Courier Settings', href: '/admin/courier-settings', icon: CogIcon }
]