import cookie from 'js-cookie'
import Router from 'next/router'
import { http } from 'utils'

const logout = () => {
  const refreshToken = cookie.get('refreshToken')
  http.post('/auth/logout', { refreshToken })
    .then(() => {
      cookie.remove('refreshToken')
      cookie.remove('accessToken')
      localStorage.removeItem('user')

      Router.push('/login')
    })
}

export default [
  { name: 'Your Profile', href: '#', onClick: () => { alert('Your Profile') } },
  { name: 'Settings', href: '#', onClick: () => { alert('Setting') } },
  { name: 'Sign out', href: '#', onClick: logout },
]