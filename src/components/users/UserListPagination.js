import _ from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import { userAction } from 'store/actions'

const Component = () => {
  const dispatch = useDispatch()
  const { userData, filters, isFetching } = useSelector((state) => state.users)
  const page = userData.page ? userData.page : 0
  const limit = userData.limit ? userData.limit : 0
  const totalPages = userData.totalPages ? userData.totalPages : 0
  const total = userData.totalResults ? userData.totalResults : 0
  const totalOf = page * limit

  const updateFilter = (count) => {
    let page = 1
    if (_.has(filters, 'page')) {
      page = filters.page
    }

    dispatch(userAction.updateFilters({ ...filters, page: page + count }))
    dispatch(userAction.fetchUsers())
  }
  
  return (
    <nav
      className="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6"
      aria-label="Pagination"
    >
      <div className="hidden sm:block">
        <p className="text-sm text-gray-700">
          Showing <span className="font-medium">{ ( page - 1) * limit + 1 }</span> to <span className="font-medium">{ totalOf > total ? total : totalOf }</span> of{' '}
          <span className="font-medium">{ total }</span> results
        </p>
      </div>
      <div className="flex-1 flex justify-between sm:justify-end">
        <button
          href="#"
          className={ `${page == 1 || isFetching ? 'opacity-50 cursor-default' : ''} relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50` }
          disabled={ page == 1 || isFetching }
          onClick={ () => updateFilter(-1) }
        >
          Previous
        </button>
        <button
          href="#"
          className={ `${page == totalPages || isFetching ? 'opacity-50 cursor-default' : ''} ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50`}
          disabled={ page == totalPages || isFetching }
          onClick={ () => updateFilter(1) }
        >
          Next
        </button>
      </div>
    </nav>
  )
}

export default Component
