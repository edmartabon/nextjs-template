import { Fragment, useState, useEffect } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { useDispatch } from 'react-redux'
import { eventBus, http, notification } from 'utils'
import { userAction } from 'store/actions'

const Component = () => {
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false)
  const [isUpdating, setIsUpdatingUser] = useState(false)
  const [fields, setFields] = useState({})
  const [fieldErrors, setFieldErrors] = useState({})

  useEffect(() => {
    eventBus.on('user.modal.update', openUserCreateModal)
    setFields({})
    setFieldErrors({})
    setIsUpdatingUser(false)
  }, [])

  const openUserCreateModal = (user) => {
    setFields(user)
    setOpen(true)
  }

  const onInputChange = (e) => {
    const { name, value } = e.target
    setFields({ ...fields, [name]: value })
  }

  const formSubmit = () => {
    document.querySelector('.btn-update-user').click()
  }

  const onFormSubmit = (e) => {
    e.preventDefault()
    setIsUpdatingUser(true)
    setFieldErrors({})
    
    const newFields = { ...fields }
    delete newFields['id']

    http.patch(`/users/${fields.id}`, newFields)
      .then(() => dispatch(userAction.fetchUsers()))
      .then(closeModal)
      .then(addNotification)
      .catch(failedUpdateUser)
      .finally(() => setIsUpdatingUser(false))
  }

  const addNotification = () => {
    notification({
      title: 'Successfully Update',
      description: 'you\'ve successfully update an user'
    })
  }

  const closeModal = () => {
    setOpen(false)
    setFields({})
    setFieldErrors({})
  }
  
  const failedUpdateUser = ({ response }) => {
    const { data, status } = response
    
    if (status == 422) {
      setFieldErrors(data.error)
    }
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed z-10 inset-0 overflow-y-auto"
        open={open}
        onClose={setOpen}
      >
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="mt-3 text-center sm:mt-0 sm:text-left">
                  <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                    Update User Information
                  </Dialog.Title>
                  <p className="mt-1 text-sm text-gray-500">Use a permanent address where you can receive mail.</p>
                  <div className="mt-2">
                    <div className="py-4">
                      <form onSubmit={ onFormSubmit } className="grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-12">
                        <div className="sm:col-span-6">
                          <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                            First name
                          </label>
                          <div className="mt-1">
                            <input
                              type="text"
                              name="firstName"
                              id="first-name"
                              required
                              autoComplete="off"
                              value={ fields.firstName || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.firstName ? 'border-red-300' : 'border-gray-300'} shadow-sm border focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md ` }
                            />
                            { fieldErrors.firstName && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.firstName }</p>
                            }
                          </div>
                        </div>

                        <div className="sm:col-span-6 lg:col-span-6">
                          <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                            Last name
                          </label>
                          <div className="mt-1">
                            <input
                              type="text"
                              name="lastName"
                              id="last-name"
                              required
                              autoComplete="off"
                              value={ fields.lastName || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.lastName ? 'border-red-300' : 'border-gray-300'} shadow-sm border focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md` }
                            />
                            { fieldErrors.lastName && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.lastName }</p>
                            }
                          </div>
                        </div>

                        <div className="sm:col-span-12">
                          <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                            Email address
                          </label>
                          <div className="mt-1">
                            <input
                              id="email"
                              // required
                              name="email"
                              type="email"
                              value={ fields.email || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.email ? 'border-red-300' : 'border-gray-300'} shadow-sm border focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md` }
                            />
                            { fieldErrors.email && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.email.replaceAll('"', '') }</p>
                            }
                          </div>
                        </div>

                        <div className="sm:col-span-12">
                          <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                            Password
                          </label>
                          <div className="mt-1">
                            <input
                              id="password"
                              name="password"
                              type="password"
                              value={ fields.password || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.password ? 'border-red-300' : 'border-gray-300'} shadow-sm border border-gray-300 focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md` }
                            />
                            { fieldErrors.password && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.password }</p>
                            }
                          </div>
                        </div>
                        <input type="submit" className="hidden btn-update-user" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <button
                  type="button"
                  tabIndex="4"
                  className={`${isUpdating ? 'opacity-50' : ''} w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm`}
                  disabled={isUpdating}
                  onClick={formSubmit}
                >
                  Update User
                </button>
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default Component
