import _ from 'lodash'
import { useDispatch } from 'react-redux'
import { eventBus } from 'utils'
import { userAction } from 'store/actions'

const Component = () => {
  const dispatch = useDispatch()

  const openUserCreateModal = () => {
    eventBus.dispatch('user.modal.create', true)
  }

  const search = _.debounce(({ target }) => {
    const filter = {...(target.value && { q: target.value })}

    dispatch(userAction.updateFilters(filter))
    dispatch(userAction.fetchUsers())
  }, 1000)

  return (
    <div className="mt-3 flex sm:mt-0 sm:ml-5">
      <div className="max-w-3xl mt-1 sm:mt-0 sm:col-span-5">
        <input
          type="text"
          name="q"
          id="q"
          placeholder="Search..."
          onChange={search}
          className="block max-w-3xl border border-gray-300 sm:rounded-lg shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm ounded-md"
        />
      </div>

      <button
        type="button"
        className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        onClick={openUserCreateModal}
      >
        Create
      </button>
    </div>
  )
}

export default Component
