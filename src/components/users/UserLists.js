import { useSelector } from 'react-redux'
import { eventBus } from 'utils'

const Component = () => {
  const { userData } = useSelector((state) => state.users)

  const openUserCreateModal = (user) => {
    eventBus.dispatch('user.modal.update', user)
  }

  const openUserDeleteModal = (user) => {
    eventBus.dispatch('user.modal.delete', user)
  }

  return (
    <table className="min-w-full divide-y divide-gray-200">
      <thead className="bg-gray-50">
        <tr>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            #
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            Name
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          > 
            Title
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            Email
          </th>
          <th scope="col" className="relative px-6 py-3">
            <span className="sr-only">Edit</span>
          </th>
        </tr>
      </thead>
      <tbody>
        { userData.results && userData.results.map((user, index) => (
            <tr key={user.id} className={index % 2 === 0 ? 'bg-white' : 'bg-gray-50'}>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{ ((userData.page * userData.limit) - userData.limit) + index + 1 }.</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{user.firstName} {user.lastName}</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">Unknown</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{user.email}</td>
              <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                <a href="#" className="text-indigo-600 hover:text-indigo-900" onClick={() => openUserCreateModal(user)}>
                  Edit
                </a>

                <a href="#" className="text-red-600 hover:text-red-900 ml-5" onClick={() => openUserDeleteModal(user)}>
                  Delete
                </a>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  )
}

export default Component
