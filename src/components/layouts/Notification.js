/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState, useEffect } from 'react'
import { Transition } from '@headlessui/react'
import { CheckCircleIcon } from '@heroicons/react/outline'
import { XIcon, XCircleIcon } from '@heroicons/react/solid'
import { eventBus } from 'utils'

const Component = () => {
  const [notifLists, setNotifLists] = useState([])

  useEffect(() => {
    eventBus.on('notification.add', addNotification)
  }, [])

  const addNotification = ({ title = '', description = '', error = false }) => {
    const id = Math.floor(Math.random() * 9999999999)
    setNotifLists(notifLists => [...notifLists, { id, title, description, error }])

    setTimeout(() => {
      removeNotification(id)
    }, 5000)
  }

  const removeNotification = (id) => {
    setNotifLists(prevNotifLists => (
      prevNotifLists.filter(x => x.id !== id)
    ))
  }

  return (
    <div
      aria-live="assertive"
      className="fixed top-0 right-0 px-4 py-6 pointer-events-none sm:p-6" 
      style={{ width: '432px' }}
    >
      { notifLists.length > 0 && notifLists.map(x => (
      <Transition
        show={true}
        key={ x.id }
        enter="transform ease-out duration-300 transition"
        enterFrom="translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
        enterTo="translate-y-0 opacity-100 sm:translate-x-0"
        leave="transition ease-in duration-100"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className="max-w-sm w-full mb-4 bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
          <div className="p-4">
            <div className="flex items-start">
              <div className="flex-shrink-0">
                { x.error ? <XCircleIcon className="h-6 w-6 text-red-400" aria-hidden="true" /> : <CheckCircleIcon className="h-6 w-6 text-green-400" aria-hidden="true" /> }
              </div>
              <div className="ml-3 w-0 flex-1 pt-0.5">
                <p className="text-sm font-medium text-gray-900">{ x.title }</p>
                <p className="mt-1 text-sm text-gray-500">{ x.description }</p>
              </div>
              <div className="ml-4 flex-shrink-0 flex">
                <button
                  className="bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={() => removeNotification(x.id)}
                >
                  <span className="sr-only">Close</span>
                  <XIcon className="h-5 w-5" aria-hidden="true" />
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* ))} */}
      </Transition>
      ))}
    
    </div>
  )
}

export default Component