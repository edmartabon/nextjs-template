import { useEffect } from 'react'
import { Provider } from 'react-redux'
import { useRouter } from 'next/router'
import SiteHeader from './Header'
import SiteSidebar from './Sidebar'
import Notification from './Notification'
import { auth } from 'utils'

const SiteLayout = ({ children }) => {
  const router = useRouter()

  useEffect(() => {
    if (!auth.isLogged()) {
      router.push('/login')
    }
  })

  return (
    <div className="min-h-screen bg-gray-100">
      <SiteHeader />
      <div className="py-10">
        <div className="max-w-3xl mx-auto sm:px-6 lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-12 lg:gap-8">
          <div className="hidden lg:block lg:col-span-3 xl:col-span-2">
            <SiteSidebar />
          </div>
          {children}
        </div>
      </div>
      <Notification />
    </div>
  )
}

export const getLayout = (page, store) => (
  <Provider store={store}>
    <SiteLayout>{page}</SiteLayout>
  </Provider>
)

export default SiteLayout;
