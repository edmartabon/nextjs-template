import Link from 'next/link'
import { useRouter } from 'next/router'
import navigation from 'commons/sidebar-list'

const SiteSidebar = () => {
  const router = useRouter()

  return (
    <nav aria-label="Sidebar" className="sticky top-4 divide-y divide-gray-300">
      <div className="pb-8 space-y-1">
        {navigation.map((item) => (
          <Link href={item.href} key={item.name}>
            <a
              className={`${router.pathname == item.href ? 'bg-gray-200 text-gray-900' : 'text-gray-600 hover:bg-gray-50'} 'group flex items-center px-3 py-2 text-sm font-medium rounded-md`}
              aria-current={router.pathname == item.href ? 'page' : undefined}
            >
              <item.icon 
                className={`${router.pathname == item.href ? 'text-gray-500' : 'text-gray-400 group-hover:text-gray-500' } flex-shrink-0 -ml-1 mr-3 h-6 w-6`} aria-hidden="true"
              />
              <span className="truncate">{item.name}</span>
            </a>
          </Link>
        ))}
      </div>
    </nav>
  )
}

export default SiteSidebar;