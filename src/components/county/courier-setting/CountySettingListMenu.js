import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { useSelector, useDispatch } from 'react-redux'
import * as types from 'store/constants/action.types'

const Component = ({ indexField }) => {
  const dispatch = useDispatch()
  const { county: modelData } = useSelector((state) => state.county)

  const updateModel = (index) => {
    modelData.fields.splice(index, 1)

    dispatch((dispatch) => {
      dispatch({
        type: types.SET_COUNTY_DATA,
        payload: modelData
      })
    })
  }

  return (
    <div className="flex items-center px-4">
      <div className="relative inline-block text-left">
        <div>
          <Menu as="div" className="relative inline-block text-left">
            {({ open }) => (
              <>
                <div>
                  <Menu.Button className="flex items-center p-2 text-gray-400 bg-gray-100 rounded-lg hover:text-gray-600 focus:outline-none">
                    <span className="sr-only">Open options</span>
                    <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"
                      aria-hidden="true">
                      <path
                        d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
                    </svg>
                  </Menu.Button>
                </div>

                <Transition
                  show={open}
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items
                    static
                    className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                  >
                    <div className="py-1">
                      <Menu.Item>
                        {({ active }) => (
                          <a
                            href="#"
                            className={`${ active ? 'bg-gray-100 text-gray-900' : 'text-gray-700' } block px-4 py-2 text-sm`}
                            onClick={ () => updateModel(indexField) }
                          >
                            Delete
                          </a>
                        )}
                      </Menu.Item>
                    </div>
                  </Menu.Items>
                </Transition>
              </>
            )}
          </Menu>
        </div>
      </div>
    </div>

  )
}

export default Component