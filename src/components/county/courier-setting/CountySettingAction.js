import _ from 'lodash'
import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { countyAction } from 'store/actions'
import * as types from 'store/constants/action.types'
import { http } from 'utils'

const Component = () => {
  const { county: modelData } = useSelector((state) => state.county)
  const [isUpdating, setIsUpdating] = useState(false)
  const router = useRouter()
  const dispatch = useDispatch()

  const fetchcountyCsv = () => {
    const { pid } = router.query
    dispatch(countyAction.fetchCountyCsv(pid))
  }

  const addFields = () => {
    modelData.fields.push({
      _id: Math.random().toString(14).slice(2),
      key: '',
      value: ''
    })

    dispatch((dispatch) => {
      dispatch({
        type: types.SET_COUNTY_DATA,
        payload: modelData
      })
    })
  }

  const updateFields = () => {
    const data = { ...modelData }
    const { id, fields } = data
    const validatedFields = fields
      .map(x => {
        const { _id, ...item } = x
        return item
      })

    setIsUpdating(true)
    http.patch(`/counties/${id}`, { fields: validatedFields })
      .finally(() => setIsUpdating(false))
  }

  return (
    <div className="mt-3 flex sm:mt-0 sm:ml-5">
      <button
        type="button"
        className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        onClick={addFields}
      >
        Add Fields
      </button>

      <button
        type="button"
        className="ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        onClick={fetchcountyCsv}
      >
        Fetch CSV
      </button>

      <button
        type="button"
        className={` ${isUpdating ? 'opacity-50': '' } ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ` }
        onClick={updateFields}
      >
        Save
      </button>
    </div>
  )
}

export default Component
