import { useSelector, useDispatch } from 'react-redux'
import CountyListMenu from './CountySettingListMenu'
import * as types from 'store/constants/action.types'

const Component = ({ item, indexField }) => {
  const dispatch = useDispatch()
  const { county: modelData } = useSelector((state) => state.county)

  const updateFields = (event, index) => {
    const { name, value } = event.target

    modelData.fields[index][name] = value
    updateModel()
  }

  const updateModel = () => {
    dispatch((dispatch) => {
      dispatch({
        type: types.SET_COUNTY_DATA,
        payload: modelData
      })
    })
  }

  return (
    <>
      <div className="flex-shrink-0 py-4 pl-4 cursor-move">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
        </svg>
      </div>
        
      <div className="mt-1 grid grid-cols-2 gap-y-6 gap-x-4 sm:grid-cols-2 w-full">
        <div className="col-span-6 sm:col-span-1">
          <select
            name="key"
            autoComplete="country"
            className="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm"
            value={ item.key }
            onChange={ (e) => updateFields(e, indexField) }
          >
            { !_.isEmpty(modelData) && modelData.rawFields.map((x, indexRawField) => (
              <option value={ x } key={ indexRawField }>{ x }</option>
            ))}
          </select>
        </div>

        <div className="col-span-6 sm:col-span-1">
          <input
            type="text"
            name="value"
            autoComplete="cc-csc"
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-gray-900 focus:border-gray-900 sm:text-sm"
            value={ item.value }
            onChange={ (e) => updateFields(e, indexField) }
          />
        </div>
      </div>
      <CountyListMenu indexField={indexField} />
    </>
  )
}

export default Component