import _ from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { countyAction } from 'store/actions'
import CountyListItem from './CountyListItem'
import * as types from 'store/constants/action.types'
import { 
  DragDropContext, 
  Droppable, 
  Draggable, 
  resetServerContext 
} from 'react-beautiful-dnd'

const Component = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [isRouteParamLoaded, setIsRouteParamLoaded] = useState(false)
  const { county: modelData, isFetchingOne } = useSelector((state) => state.county)

  useEffect(() => dispatch({
    type: types.SET_COUNTY_DATA,
    payload: {}
  }), [])
  
  useEffect(() => {    
    if (router.query.pid) {
      setIsRouteParamLoaded(true)
    }

    if (isRouteParamLoaded && _.isEmpty(modelData) && !isFetchingOne) {
      dispatch(countyAction.fetchCounty(
        router.query.pid
      ))
    }
  })

  const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: 'none',
    background: isDragging ? '#D1D5DB' : '#FFF',
    borderRadius: '0.5rem',
    borderBottomWidth: '1px',
  
    // styles we need to apply on draggables
    ...draggableStyle
  });

  const onDragEnd = (result) => {
    const { destination, source } = result
    const indexDestination = destination.index
    const indexSource = source.index

    modelData.fields.splice(
      indexDestination, 0,
      modelData.fields.splice(indexSource, 1)[0]
    )

    dispatch((dispatch) => {
      dispatch({
        type: types.SET_COUNTY_DATA,
        payload: modelData
      })
    })
  }

  resetServerContext()

  return (
    <div className="space-y-6">
      <ul className="divide-y divide-gray-100 shadow-custom">
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                { !_.isEmpty(modelData) && modelData.fields
                  .map((item, indexField) => (
                    <Draggable key={item._id} draggableId={item._id} index={indexField}>
                      {(provided, snapshot) => (
                        <li className="flex space-x-3 py-2 group bg-white border-b border-gray-100 rounded-lg mb-4"
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <CountyListItem 
                            modelData={modelData} 
                            item={ item } 
                            indexField={ indexField } 
                            key={ indexField }
                            
                          />
                        </li>
                      )}
                    </Draggable>
                ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </ul>
    </div>
  );
}

export default Component