import Link from 'next/link'
import { useSelector } from 'react-redux'
import { eventBus } from 'utils'

const Component = () => {
  const { countyData: data } = useSelector((state) => state.county)

  const openUpdateModal = (model) => {
    eventBus.dispatch('county.modal.update', model)
  }

  const openDeleteModal = (model) => {
    eventBus.dispatch('county.modal.delete', model)
  }

  return (
    <table className="min-w-full divide-y divide-gray-200">
      <thead className="bg-gray-50">
        <tr>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            #
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            Name
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          > 
            Total Fields
          </th>
          <th
            scope="col"
            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
          >
            Link
          </th>
          <th scope="col" className="relative px-6 py-3">
            <span className="sr-only">Edit</span>
          </th>
        </tr>
      </thead>
      <tbody>
        { data.results && data.results.map((model, index) => (
            <tr key={model.id} className={index % 2 === 0 ? 'bg-white' : 'bg-gray-50'}>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{ ((data.page * data.limit) - data.limit) + index + 1 }.</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{model.name}</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{model.fields.length}</td>
              <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{model.link}</td>
              <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                <Link href={`/admin/courier-settings/${model.id}`}>
                  <a className="text-blue-600 hover:text-blue-900 mr-4">
                    Mapper
                  </a>
                </Link>

                <a href="#" className="text-indigo-600 hover:text-indigo-900" onClick={() => openUpdateModal(model)}>
                  Edit
                </a>

                <a href="#" className="text-red-600 hover:text-red-900 ml-5" onClick={() => openDeleteModal(model)}>
                  Delete
                </a>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  )
}

export default Component
