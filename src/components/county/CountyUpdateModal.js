import { Fragment, useState, useEffect } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { useDispatch } from 'react-redux'
import { eventBus, http, notification } from 'utils'
import { countyAction } from 'store/actions'

const Component = () => {
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false)
  const [isUpdating, setIsUpdating] = useState(false)
  const [fields, setFields] = useState({})
  const [fieldErrors, setFieldErrors] = useState({})

  useEffect(() => {
    eventBus.on('county.modal.update', openUpdateModal)
    setFields({})
    setFieldErrors({})
    setIsUpdating(false)
  }, [])

  const openUpdateModal = (county) => {
    setFields(county)
    setOpen(true)
  }

  const onInputChange = (e) => {
    const { name, value } = e.target
    setFields({ ...fields, [name]: value })
  }

  const formSubmit = () => {
    document.querySelector('.btn-update').click()
  }

  const onFormSubmit = (e) => {
    e.preventDefault()
    setIsUpdating(true)
    setFieldErrors({})
    
    const newFields = { ...fields }
    delete newFields['id']

    http.patch(`/counties/${fields.id}`, newFields)
      .then(() => dispatch(countyAction.fetchCounties()))
      .then(closeModal)
      .then(addNotification)
      .catch(failedUpdate)
      .finally(() => setIsUpdating(false))
  }

  const addNotification = () => {
    notification({
      title: 'Successfully Update',
      description: 'you\'ve successfully update a county'
    })
  }

  const closeModal = () => {
    setOpen(false)
    setFields({})
    setFieldErrors({})
  }
  
  const failedUpdate = ({ response }) => {
    const { data, status } = response
    
    if (status == 422) {
      setFieldErrors(data.error)
    }
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        static
        className="fixed z-10 inset-0 overflow-y-auto"
        open={open}
        onClose={setOpen}
      >
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="mt-3 text-center sm:mt-0 sm:text-left">
                  <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                    Update County Information
                  </Dialog.Title>
                  <div className="mt-2">
                    <div className="py-4">
                      <form onSubmit={ onFormSubmit } className="grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-12">
                        <div className="sm:col-span-12">
                          <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                            County Name
                          </label>
                          <div className="mt-1">
                            <input
                              id="name"
                              // required
                              name="name"
                              type="text"
                              value={ fields.name || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.name ? 'border-red-300' : 'border-gray-300'} shadow-sm border focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md` }
                            />
                            { fieldErrors.name && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.name.replaceAll('"', '') }</p>
                            }
                          </div>
                        </div>

                        <div className="sm:col-span-12">
                          <label htmlFor="link" className="block text-sm font-medium text-gray-700">
                            Link
                          </label>
                          <div className="mt-1">
                            <input
                              id="link"
                              // required
                              name="link"
                              type="text"
                              value={ fields.link || '' }
                              onChange={ onInputChange }
                              className={ `${fieldErrors.link ? 'border-red-300' : 'border-gray-300'} shadow-sm border focus:ring-gray-500 focus:border-gray-500 block w-full sm:text-sm rounded-md` }
                            />
                            { fieldErrors.link && 
                              <p className="mt-2 text-xs text-red-500">{ fieldErrors.link.replaceAll('"', '') }</p>
                            }
                          </div>
                        </div>
                        
                        <input type="submit" className="hidden btn-update" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <button
                  type="button"
                  tabIndex="4"
                  className={`${isUpdating ? 'opacity-50' : ''} w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm`}
                  disabled={isUpdating}
                  onClick={formSubmit}
                >
                  Update County
                </button>
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

export default Component
