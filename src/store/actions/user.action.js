import * as types from 'store/constants/action.types'
import { http } from 'utils'

export default {
  fetchUsers: () => (dispatch, getState) => {
    const { users } = getState()
    const query = new URLSearchParams(users.filters).toString()

    // Set isFetching to true
    dispatch({ 
      type: types.FETCHING_USER_DATA, 
      payload: true 
    })

    return http.get(`/users?${query}`)
      .then(({ data }) => {
        dispatch({ 
          type: types.SET_USER_DATA, 
          payload: data 
        })
        return data
      })
      .finally(() => {
        dispatch({ 
          type: types.FETCHING_USER_DATA, 
          payload: false 
        })
      })
  },

  updateFilters: data => dispatch => {
    dispatch({ 
      type: types.SET_USER_FILTER, 
      payload: data 
    })
  }

}