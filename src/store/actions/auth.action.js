import * as types from 'store/constants/action.types'

export default {
  setUser: user => dispatch => {
    dispatch({ type: types.SET_AUTH,  payload: user })
  }
}