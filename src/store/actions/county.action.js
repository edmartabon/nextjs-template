import * as types from 'store/constants/action.types'
import { http } from 'utils'

export default {
  fetchCounties: () => (dispatch, getState) => {
    const { county } = getState()
    const query = new URLSearchParams(county.filters).toString()

    // Set isFetching to true
    dispatch({ 
      type: types.FETCHING_COUNTIES_DATA, 
      payload: true 
    })

    return http.get(`/counties?${query}`)
      .then(({ data }) => {
        dispatch({ 
          type: types.SET_COUNTIES_DATA, 
          payload: data 
        })
        return data
      })
      .finally(() => {
        dispatch({ 
          type: types.FETCHING_COUNTIES_DATA, 
          payload: false 
        })
      })
  },

  fetchCounty: (id) => (dispatch) => {

    // Set isFetching to true
    dispatch({ 
      type: types.FETCHING_COUNTY_DATA, 
      payload: true 
    })

    return http.get(`/counties/${id}`)
      .then(({ data }) => {
        dispatch({ 
          type: types.SET_COUNTY_DATA, 
          payload: data 
        })
        return data
      })
      .finally(() => {
        dispatch({ 
          type: types.FETCHING_COUNTY_DATA, 
          payload: false 
        })
      })
  },
  
  fetchCountyCsv: (id) => (dispatch) => {
    return http.get(`/counties/${id}/fetch-csv`)
      .then(({ data }) => {
        dispatch({ 
          type: types.SET_COUNTY_CSV, 
          payload: data 
        })
        return data
      })
  },

  updateFilters: data => dispatch => {
    dispatch({ 
      type: types.SET_COUNTY_FILTER, 
      payload: data 
    })
  }
}