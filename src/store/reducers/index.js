import { combineReducers } from 'redux'
import auth from './auth.reducer'
import users from './user.reducer'
import county from './county.reducer'

export default combineReducers({
  auth, 
  users,
  county,
})