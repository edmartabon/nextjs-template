import * as types from 'store/constants/action.types'

const initialState = {
  user: {}
}

export default function Auth(state = initialState, action) {
  switch(action.type) {
    case types.SET_AUTH:
      return { ...state, user: action.payload }
    default:
      return { ...state }
  }
}
