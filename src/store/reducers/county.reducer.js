import * as types from 'store/constants/action.types'

const initialState = {
  countyData: {},
  county: {},
  filters: {},
  isFetching: false,
  isFetchingOne: false,
}

export default function County(state = initialState, action) {
  switch(action.type) {
    case types.FETCHING_COUNTIES_DATA:
      return { ...state, isFetching: action.payload }
    case types.SET_COUNTIES_DATA:
      return { ...state, countyData: action.payload }
    case types.SET_COUNTY_FILTER:
      return { ...state, filters: action.payload }
    case types.SET_COUNTY_CSV:
      return { ...state, county: action.payload }
    case types.FETCHING_COUNTY_DATA:
      return { ...state, isFetchingOne: action.payload }
    case types.SET_COUNTY_DATA:
        return { ...state, county: action.payload }
    default:
      return { ...state }
  }
}
