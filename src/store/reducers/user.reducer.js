import * as types from 'store/constants/action.types'

const initialState = {
  userData: {},
  filters: {},
  isFetching: false,
}

export default function User(state = initialState, action) {
  switch(action.type) {
    case types.FETCHING_USER_DATA:
      return { ...state, isFetching: action.payload }
    case types.SET_USER_DATA:
      return { ...state, userData: action.payload }
    case types.SET_USER_FILTER:
      return { ...state, filters: action.payload }  
    default:
      return { ...state }
  }
}
