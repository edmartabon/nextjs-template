import React from 'react'
import { Provider } from 'react-redux'
import { useStore } from 'store'
import SiteLayout from 'components/layouts/Admin'
import 'tailwindcss/tailwind.css'

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState)
  const getLayout =
    Component.getLayout || (page => <SiteLayout children={page} />)

  if (typeof Component.getLayout == 'undefined') {
    return (
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    )
  }

  return getLayout(<Component {...pageProps } />, store)
}

