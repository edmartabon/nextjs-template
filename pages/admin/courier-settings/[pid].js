import { getLayout } from 'components/layouts/Admin'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { countyAction } from 'store/actions'
import { 
  CountySettingAction,
  CountySettingLists
} from 'components/county'

const Component = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(countyAction.fetchCounties())
  }, [])

  return (
    <main className="lg:col-span-10">
      <div className="px-4 sm:px-0">
        <main className="flex-1 relative pb-8 z-0">
          <div className="pb-5  border-gray-200 sm:flex sm:items-center sm:justify-between">
            <h2 className="max-w-6xl pb-4 text-lg leading-6 font-medium text-gray-900">
              Courier Setting
            </h2>
            <CountySettingAction />
          </div>

          <CountySettingLists />
        </main>
      </div>
    </main>
  )
}

Component.getLayout = getLayout

export default Component 
