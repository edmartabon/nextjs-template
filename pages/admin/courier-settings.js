import { getLayout } from 'components/layouts/Admin'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import * as action from 'store/actions'
import { 
  CountyLists,
  CountyPagination,
  CountyCreateModal,
  CountyUpdateModal,
  CountyDeleteModal,
  CountyActions
} from 'components/county'

const Users = () => {
  const dispatch = useDispatch()
  const countyStoreAction = action.countyAction

  useEffect(() => {
    dispatch(countyStoreAction.fetchCounties())
  }, [])

  return (
    <main className="lg:col-span-10">
      <div className="px-4 sm:px-0">
        <main className="flex-1 relative pb-8 z-0">
          <div className="pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
            <h2 className="max-w-6xl pb-4 text-lg leading-6 font-medium text-gray-900">
              Courier Setting
            </h2>
            <CountyActions />
          </div>

          <div className="hidden sm:block">
            <div className="max-w-6xl">
              <div className="flex flex-col">
                <div className="-my-2 sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                      <CountyLists />
                       <CountyPagination />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
      <CountyCreateModal />
      <CountyUpdateModal />
      <CountyDeleteModal />
    </main>
  )
}

Users.getLayout = getLayout

export default Users
