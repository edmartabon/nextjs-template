import Link from 'next/link'
import moment from 'moment'
import cookie from 'js-cookie'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import React, { useState, useEffect } from 'react'
import { authAction } from 'store/actions'
import { auth, http } from 'utils'

export default function Login() {
  const router = useRouter()
  const dispatch = useDispatch()

  const [isLoading, setIsLoading] = useState(false)
  const [isAuthFailed, setIsAuthFailed] = useState(false)

  const redirectTo = '/'

  useEffect(() => {
    if (auth.isLogged()) {
      router.push(redirectTo)
    }
  }, [])

  const handleSubmit = (e) => {
    e.preventDefault()
    setIsLoading(true)
    setIsAuthFailed(false)

    http.post('/auth/login', {
      email: e.target.email.value,
      password: e.target.password.value
    })
      .then(authSuccess)
      .catch(() => setIsAuthFailed(true))
      .finally(() => {
        e.target.password.value = ''
        setIsLoading(false)
      })
  }

  const authSuccess = ({ data }) => {
    const { tokens } = data
    const { access, refresh } = tokens

    dispatch(authAction.setUser(data.user))

    const accessExpiredToken = moment().add(access.expires, 'seconds').toDate()
    const refreshExpiredToken = moment().add(refresh.expires, 'seconds').toDate()

    cookie.set('accessToken', access.token, { expires: accessExpiredToken })
    cookie.set('refreshToken', refresh.token, { expires: refreshExpiredToken })

    localStorage.setItem('user', JSON.stringify(data.user))

    router.push(redirectTo)
  }

  return (
    <div className="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Sign in to your account</h2>
        <p className="mt-2 text-center text-sm text-gray-600 max-w">
          Or{' '}
          <Link href="/" className="font-medium text-indigo-600 hover:text-indigo-500">
            Create your account
          </Link>
        </p>
      </div>

      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          { isAuthFailed && 
            <div className="rounded-md bg-red-50 p-4 mb-4 text-center">
              <div className="ml-3">
                <h3 className="text-sm font-medium text-red-800">Email or password is incorrect</h3>
              </div>
            </div>
          }

          <form className="space-y-6" onSubmit={handleSubmit}>
            <div>
              <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                Email address
              </label>  
              <div className="mt-1">
                <input
                  id="email"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>
            </div>

            <div>
              <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                Password
              </label>
              <div className="mt-1">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                />
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <input
                  id="remember_me"
                  name="remember_me"
                  type="checkbox"
                  className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
                />
                <label htmlFor="remember_me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              <div className="text-sm">
                <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                  Forgot your password?
                </a>
              </div>
            </div>

            <div>
              <button
                type="submit"
                className={`w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${isLoading ? 'opacity-50' : ''}` }
                disabled={isLoading}
              >
                Sign in
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  )
}